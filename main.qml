import QtQuick 2.6
import QtQuick.Window 2.2

Window {
    visible: true
    id:root
    width: 500
    height: 500
    title: "*** Tembel Göz Geliştirme ***"

    property bool running: false

    property int xpos
    property int ypos

    Rectangle{
        id:body
        height: root.height
        width: root.width
        anchors.centerIn: root
        Image {
            id: background
            source: "files/images/donel5.jpg"
            anchors.centerIn: body
            fillMode: Image.PreserveAspectFit
           // width: 1350
            RotationAnimation on rotation {
                from: 0
                to: -360
                duration: 1000
                running: root.running
                loops: Animation.Infinite
            }
            /*
            MouseArea{
                anchors.fill: background
               onClicked: {
                    if (root.running === false)
                        root.running = true
                    else
                        root.running = false
                }
            } */
        }
        Image {
            id: smile
            source: "files/images/esek2.png"
            fillMode: Image.PreserveAspectFit
            anchors.centerIn: body
        }
        ////////////

        Canvas {
            id: myCanvas
            anchors.fill: parent

            onPaint: {
                var ctx = getContext('2d')
                ctx.fillStyle = "red"
                ctx.fillRect(root.xpos-1, root.ypos-1, 10, 10)

            }

            MouseArea{
                anchors.fill: parent

                 onClicked: {
                     if (root.running === false)
                         root.running = true
                     //else
                     //    root.running = false
                 }

                onPressed: {
                    root.xpos = mouseX
                    root.ypos = mouseY
                    myCanvas.requestPaint()
                }
                onMouseXChanged: {
                    root.xpos = mouseX
                    root.ypos = mouseY
                    myCanvas.requestPaint()
                }
                onMouseYChanged: {
                    root.xpos = mouseX
                    root.ypos = mouseY
                    myCanvas.requestPaint()
                }
            }

        }


        //////////////////
    }  // rect
} //window
